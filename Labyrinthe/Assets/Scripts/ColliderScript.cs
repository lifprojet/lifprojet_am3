﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderScript : MonoBehaviour 
{
	Collider myCollider;

	void Awake()
	{
		myCollider = GetComponent<Collider>();
		myCollider.isTrigger = true;
	}
}

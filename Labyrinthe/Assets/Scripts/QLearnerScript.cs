﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace QLearner
{
    public class QLearnerScript
    {
        private List<float[]> States;
        private List<float[]> Actions;

        private float[] previousState;

        private int actionPicked;
        private float actionValue;

        private float learningRate;
        private float discountFactor;
        private bool firstIteration;
        private System.Random random = new System.Random();

        public QLearnerScript()
        {
            States = new List<float[]>();
            Actions = new List<float[]>();

            learningRate = 0.9f;
            discountFactor = 0.9f;

            firstIteration = true;
        }

        public void updateQValue(float[] statePicked, float reward)
        {
            for (int i = 0; i < States.Count; i++)
            {
                float[] state = States.ElementAt(i);
                float[] actions = Actions.ElementAt(i);

                if (state.SequenceEqual(statePicked))
                {
                    actionValue = actions.Max();
                }
            }

            for (int i = 0; i < States.Count; i++)
            {
                float[] state = States.ElementAt(i);
                float[] actions = Actions.ElementAt(i);

                if (state.SequenceEqual(previousState))
                {
                    actions[actionPicked] = (actions[actionPicked] + learningRate * (reward + discountFactor * actionValue - actions[actionPicked]));
                }
            }
        }

        public int main(float[] currentState, float reward)
        {
            if (!firstIteration)
            {
                updateQValue(currentState, reward);

                if (learningRate > 0f)
                {
                    learningRate -= 0.0000009f;
                } else
                {
                    learningRate = 0.0000000001f;
                }
            }
            else
            {
                firstIteration = false;
            }

            previousState = currentState;

            bool newState = true;
            if (States.Count > 0)
            {
                for (int i = 0; i < States.Count; i++)
                {
                    float[] state = States.ElementAt(i);
                    float[] actions = Actions.ElementAt(i);

                    if (state.SequenceEqual(currentState))
                    {
                        newState = false;
                        actionPicked = Array.IndexOf(actions, actions.Max());
                        return actionPicked;
                    }
                }
            }

            if (newState)
            {
                States.Add(currentState);
                Actions.Add(new float[4]);
            }

            actionPicked = random.Next(0, 4);
            return actionPicked;
        }
    }
}
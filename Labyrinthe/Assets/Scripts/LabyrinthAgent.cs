﻿using System.Collections.Generic;
using UnityEngine;
using MLAgents;
using System.Collections;

public class LabyrinthAgent : Agent
{
    Rigidbody rBody;

    void Start()
    {
        rBody = GetComponent<Rigidbody>();
    }

    public Transform target;

    private int agentValue = 0;
    private int checkpointsValue = 20;
    private bool targetReach = false;
    private bool wallCollision = false;

    public override void AgentReset()
    {
        this.transform.position = new Vector3(18f, 0.5f, -18f);
        this.rBody.angularVelocity = Vector3.zero;
        this.rBody.velocity = Vector3.zero;
    }

    public override void CollectObservations()
    {

        AddVectorObs(Vector3.Distance(target.position, this.transform.position));
        AddVectorObs((this.transform.position.x + 20) / 40);
        AddVectorObs((this.transform.position.z + 20) / 40);
    }

    public override void AgentAction(float[] vectorAction, string textAction)
    {
        if (targetReach)
        {
            AddReward(10.0f);
            targetReach = false;
            Done();
        }

        if (wallCollision)
        {
            AddReward(-1.0f);
            wallCollision = false;
            Done();
        }

        AddReward(-0.001f);

        int movement = Mathf.FloorToInt(vectorAction[0]);

        switch (movement)
        {
            case 0:
                rBody.MovePosition(transform.position + transform.forward);
                break;
            case 1:
                rBody.MovePosition(transform.position - transform.right);
                break;
            case 2:
                rBody.MovePosition(transform.position - transform.forward);
                break;
            case 3:
                rBody.MovePosition(transform.position + transform.right);
                break;
            default: break;
        }
        
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.name.Contains("Target"))
        {
            targetReach = true;
        }
        else if (col.gameObject.name.Contains("Checkpoint"))
        {
            agentValue++;
            targetReach = true;
            if (agentValue == checkpointsValue)
            {
                Physics.IgnoreCollision(col, GetComponent<Collider>());
                agentValue = 0;
            }
        }

        if (col.gameObject.name.Contains("Wall"))
        {
            wallCollision = true;
        }
    }
}
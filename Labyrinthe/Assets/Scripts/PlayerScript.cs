﻿using UnityEngine;
using QLearner;
using System.IO;
using System;

public class PlayerScript : MonoBehaviour
{
	Rigidbody rBody;

    public Transform mlAgent;

    private QLearnerScript QL = new QLearnerScript();
    private int targetDone = 0;
    private int targetTries = 20;
    private float reward = 0f;

    void Start()
	{
        Physics.IgnoreCollision(mlAgent.GetComponent<Collider>(), GetComponent<Collider>());
		rBody = GetComponent<Rigidbody>();
		rBody.useGravity = false;
		rBody.freezeRotation = true;
	}

	void Update()
	{
        int action = QL.main(new float[] {transform.position.x, transform.position.z}, reward);
		reward = -0.001f;

        switch (action)
        {
            case 0: rBody.MovePosition(transform.position + transform.forward); break;
            case 1: rBody.MovePosition(transform.position - transform.forward); break;
            case 2: rBody.MovePosition(transform.position + transform.right); break;
            case 3: rBody.MovePosition(transform.position - transform.right); break;
            default: break;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        transform.position = new Vector3(18f, 0.5f, -18f);
        
        if (col.gameObject.name.Equals("Target"))
        {
            reward += 10f;
        } else if (col.gameObject.name.Contains("Checkpoint"))
        {
            reward += 0.1f;

            targetDone ++;
            if (targetDone == targetTries)
            {
                Physics.IgnoreCollision(col, GetComponent<Collider>());
                targetDone = 0;
            }
        }
        else
        {
            reward -= 1f;
        }
	}
}

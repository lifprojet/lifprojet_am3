# Unity et Apprentissage Machine

Notre projet porte (comme son nom l'indique) sur l'apprentissage machine sous Unity.
Notre but durant ce projet a été de créer une Intelligence Artificielle (IA) capable de résoudre un labyrinthe plutôt simple.

## Installation

Pour le bon fonctionnement de notre projet, il est fortement recommandé d'utiliser Windows.
Notre projet requiert l'installation de Unity, Ml-Agents et Tensorflow.

### Utilisateurs Windows

* [Unity](https://store.unity.com/download?ref=personal)
* [Ml-agents](https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Installation-Windows.md)
* Pour Tensorflow nous recommandons de suivre l'installation d'[Anaconda](https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Installation-Windows.md) (comprise dans celle de Ml-agents)


### Utilisateurs Linux

* Unity :
```bash
pip install unity-editor
```
* [Ml-agents](https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Installation.md)
* Tensor-flow :
```bash
pip install tensorflow
```

## Comment ça marche

Tout d'abord, lancez Unity puis ouvrez le projet en sélectionnant le dossier "Labyrinthe".
Lancez ensuite un terminal de commande (si vous avez installé Anaconda, lancez Anaconda Prompt).

Exécuter ensuite les commandes suivantes :
```bash
activate ml-agents
mlagents-learn {chemin vers le fichier trainer_config.yaml} --run-id=run0 --train
```

Ensuite appuyez sur le boutton play et admirez.